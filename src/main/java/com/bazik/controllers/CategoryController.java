package com.bazik.controllers;

import com.bazik.dtos.CategoryDTO;
import com.bazik.dtos.ProductDTO;
import com.bazik.models.Category;
import com.bazik.repositories.ICategoryRepository;
import com.bazik.services.CategoryService;
import com.bazik.services.ICategoryService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("${api.prefix}/categories")
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;

    @PostMapping
    public ResponseEntity<?> createCategory(@Valid @RequestBody CategoryDTO categoryDTO, BindingResult result) {

        try {
            if (result.hasErrors()) {
                List<String> errors = result.getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
                return ResponseEntity.badRequest().body(errors);
            }
            categoryService.createCategory(categoryDTO);
            return ResponseEntity.ok("Category added successfully");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<List<Category>> getCategorys() {
        List<Category> categories = categoryService.getAllCategories();
        return ResponseEntity.ok(categories);
    }

    @GetMapping("/{id}")
    public ResponseEntity<String> getCategoryById(@PathVariable("id") long categoryId) {
        categoryService.getCategoryById(categoryId);
        return ResponseEntity.ok("getProduct By id here");
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> putCategory(@PathVariable("id") long categoryId, @RequestBody CategoryDTO categoryDTO) {
        categoryService.updateCategory(categoryId,categoryDTO);
        return ResponseEntity.ok("update category");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> DeleteCategory(@PathVariable("id") long categoryId) {
        categoryService.deleteCategory(categoryId);
        return ResponseEntity.ok("delete category");
    }
}
