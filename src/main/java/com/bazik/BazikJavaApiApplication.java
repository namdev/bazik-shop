package com.bazik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BazikJavaApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BazikJavaApiApplication.class, args);
    }

}
