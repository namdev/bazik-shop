package com.bazik.services;

import com.bazik.dtos.UserDTO;
import com.bazik.models.User;

public interface IUserService {
    User createUser(UserDTO userDTO);

    String login(String phone, String password);
}
